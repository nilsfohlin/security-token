import { environemnt } from './config.js';
import { Postchain, User, AuthDescriptor, op, Blockchain, ConnectionClient} from 'ft3-lib';
import pcl from 'postchain-client';

interface Options{
    blockchainRID: string;
    blockchainURI: string;
}

class security_token {

    private _blockchain!: Blockchain;
    private _gtx!: ConnectionClient;

    constructor (options?: Options) {
        if (options && options.blockchainRID) {
            environemnt.blockchainRID = options.blockchainRID;
        }
        if (options && options.blockchainURI) {
            environemnt.blockchainUrl = options.blockchainURI;
        }
      }

    public async init() {
        const chainId = Buffer.from(environemnt.blockchainRID, 'hex');
        this._blockchain = await new Postchain(environemnt.blockchainUrl).blockchain(chainId);
        const rest = pcl.restClient.createRestClient(environemnt.blockchainUrl, environemnt.blockchainRID, 5) // What does this 5 do?!?!
        this._gtx = pcl.gtxClient.createClient(rest,chainId,[]);
    }

    public async dev(auth: AuthDescriptor, _user: User) {
        await this._blockchain.call(op('ft3.dev_register_account', auth), _user);
    }
    /**
     * Only root can add a new platfrom provider, a platform provider is the top user of the eco-system 
     * The account cannot be registered as another type of user (pp/admin/manager/investor/etc)
     * 
     * @param _acc Account to add as platform provider
     * @param _root signer of tx
     */
    public async root_add_platform_provider( _acc: Buffer, _root: User){try{
            await this._blockchain.call(op('security_token.root_add_platform_provider', _acc), _root);
        }catch(e){throw e;}}
       
    /**
     * Only a platform provider can add a platfrom. A platfrom is the top entity that all others belong to.
     * Default, platform.operable == false.
     * 
     * @param _pp_acc_id platform provider account id
     * @param _pp_auth_id platform provider account authdescriptor id
     * @param _name platform name
     * @param _meta meta-data, json string example: '{"foo":"bar"}'
     * @param _pp_user signer of tx
     */
    public async pp_add_platform( _pp_acc_id: Buffer, _pp_auth_id: Buffer, _name: string, _meta: string, _pp_user: User){try{
            await this._blockchain.call(op('security_token.add_platform', _pp_acc_id, _pp_auth_id, _name, _meta), _pp_user);
        }catch(e){throw e;}}

    /**
     * Only platform provider can set platform.operable == false/true 
     * "Setup" of user roles etc can be done even if platform.operable == false, 
     * to transfer shares, platform has to be operable == true
     * 
     * @param _pp_acc_id platform provider account id
     * @param _pp_auth_id platform provider account authdescriptor id
     * @param _platform platfrom id
     * @param _operable enable/disable platform, boolean
     * @param _pp_user signer of tx
     */
    public async pp_platfrom_operable( _pp_acc_id: Buffer, _pp_auth_id: Buffer, _platform: number, _operable: boolean, _pp_user: User){try{
            await this._blockchain.call(op('security_token.platform_operable', _pp_acc_id, _pp_auth_id, _platform, _operable), _pp_user);
        }catch(e){throw e;}}
    
    /**
     * Only a platform provider can add a admin and only to a platfrom that they have created. 
     * The account cannot be registered as another type of user (pp/admin/manager/investor/etc)
     * Default, admin.operable == false  
     * 
     * @param _pp_acc_id platform provider account id
     * @param _pp_auth_id platform provider account authdescriptor id
     * @param _admin_id account id to add as admin for a platform
     * @param _platform platfrom id
     * @param _pp_user signer of tx
     */
    public async pp_add_platform_admin( _pp_acc_id: Buffer, _pp_auth_id: Buffer, _admin_id: Buffer, _platform: number, _pp_user: User){try{
            await this._blockchain.call(op('security_token.add_platform_admin', _pp_acc_id, _pp_auth_id, _admin_id, _platform), _pp_user);
        }catch(e){throw e;}}

    /**
     * Only a platform provider can alter admin.operable == true/false
     * An admin with operable == false can not add/alter other entities
     * 
     * @param _pp_acc_id platform provider account id
     * @param _pp_auth_id platform provider account authdescriptor id
     * @param _admin_id admin account id
     * @param _operable enable/disable admin, boolean
     * @param _pp_user signer of tx
     */
    public async pp_platform_admin_operable( _pp_acc_id: Buffer, _pp_auth_id: Buffer, _admin_id: Buffer, _operable: boolean, _pp_user: User){try{
            await this._blockchain.call(op('security_token.platform_admin_operable', _pp_acc_id, _pp_auth_id, _admin_id, _operable), _pp_user);
        }catch(e){throw e;}}


    /**
     * A platform provider might have to suspend an issuing entity. Hence, issuing_entity.suspended can only be altered by platform provider.
     * Issuing_entity.operable can be altered by both platform provider and platform admin.
     * 
     * @param _admin_acc_id platform provider account id
     * @param _admin_auth_id platform provider account authdescriptor id
     * @param _issuing_entity issuing entity id
     * @param _suspended allow/stop an issuing entity, boolean
     * @param _pp_user signer of tx
     */
    public async pp_issuing_entity_suspended( _pp_acc_id: Buffer, _pp_auth_id: Buffer, _issuing_entity: number, _suspended: boolean, _pp_user: User){try{
            await this._blockchain.call(op('security_token.issuing_entity_suspended', _pp_acc_id, _pp_auth_id, _issuing_entity, _suspended), _pp_user);
        }catch(e){throw e;}}

    /**
     * Only platform admin who is operable can add issuint entities 
     * Default  issuing_entity.operable == false
     *          issuint_entity.suspended == false
     * 
     * SupermManager/Manager/Editor and assets belongs to issuing_entity issuing entity can be seen as a company
     * 
     * @param _admin_acc_id platform admin account id
     * @param _admin_auth_id platform admin account authdescriptor id
     * @param _name issuing entity name
     * @param _meta issuint entity meta, json string, example: '{"foo":"bar"}'
     * @param _admin_user signer of tx
     */
    public async admin_add_issuing_entity( _admin_acc_id: Buffer, _admin_auth_id: Buffer, _name: string, _meta: string, _admin_user: User){try{
            await this._blockchain.call(op('security_token.add_issuing_entity', _admin_acc_id, _admin_auth_id, _name, _meta), _admin_user);
        }catch(e){throw e;}}

    /**
     * Only platform admin who is operable can set issuing_entity.operable == false/true 
     * "Setup" of user roles etc can be done even if issuing_entity.operable == false, 
     * to transfer shares, issuing_entity has to be operable == true
     * 
     * @param _admin_acc_id platform admin account id
     * @param _admin_auth_id platform admin account authdescriptor id
     * @param _issuing_entity issuing entity id
     * @param _operable enable/disable issuing entity, boolean
     * @param _admin_user signer of tx
     */
    public async admin_issuing_entity_operable( _admin_acc_id: Buffer, _admin_auth_id: Buffer, _issuing_entity: number, _operable: boolean, _admin_user: User){try{
            await this._blockchain.call(op('security_token.issuing_entity_operable', _admin_acc_id, _admin_auth_id, _issuing_entity, _operable), _admin_user);
        }catch(e){throw e;}}

    /**
     * A platform admin who is operable can add issuing entity representative, default operable == false
     * The account cannot be registered as another type of user (pp/admin/manager/investor/etc)
     * 
     * SuperManager: can add "SuperManager"/"Manager"/"Editor", add assets/alter assets/transfer assets
     * Manager:      can add assets/alter assets/transfer assets
     * Editor:       can transfer assets
     * 
     * @param _admin_acc_id platform admin account id
     * @param _admin_auth_id platform admin account authdescriptor id
     * @param _rep_acc_id issuing entity representative account id
     * @param _issuing_entity issuing entity id
     * @param _rep_title "SuperManager"/"Manager"/"Editor"
     * @param _admin_user signer of tx
     */
    public async admin_add_ie_rep( _admin_acc_id: Buffer, _admin_auth_id: Buffer, _rep_acc_id: Buffer, _issuing_entity: number, _rep_title: string, _admin_user: User){try{
            await this._blockchain.call(op('security_token.admin_add_ie_rep', _admin_acc_id, _admin_auth_id, _rep_acc_id, _issuing_entity, _rep_title), _admin_user);
        }catch(e){throw e;}}

    /**
     * A platform admin who is operable can alter issuing entity representative.operable == true/false
     * 
     * @param _admin_acc_id platform admin account id
     * @param _admin_auth_id platform admin account authdescriptor id
     * @param _rep_acc_id issuing entity representative account id
     * @param _operable enable/disable issuing entity representative, boolean
     * @param _admin_user signer of tx
     */
    public async admin_ie_rep_operable( _admin_acc_id: Buffer, _admin_auth_id: Buffer, _rep_acc_id: Buffer, _operable: boolean, _admin_user: User){try{
            await this._blockchain.call(op('security_token.admin_ie_rep_operable', _admin_acc_id, _admin_auth_id, _rep_acc_id, _operable), _admin_user);
        }catch(e){throw e;}}

    /**
     * Only a platform admin who is operable can alter issuing entity suspended.operable == true/false
     * If an issuing entity representative have to be suspended but the SuperManager of that entity does not agree
     * 
     * @param _admin_acc_id platform admin account id
     * @param _admin_auth_id platform admin account authdescriptor id
     * @param _rep_acc_id issuing entity representative account id
     * @param _suspended allow/stop an issuing entity representative, boolean
     * @param _admin_user signer of tx
     */
    public async admin_ie_rep_suspended( _admin_acc_id: Buffer, _admin_auth_id: Buffer, _rep_acc_id: Buffer, _suspended: boolean, _admin_user: User){try{
            await this._blockchain.call(op('security_token.admin_ie_rep_suspended', _admin_acc_id, _admin_auth_id, _rep_acc_id, _suspended), _admin_user);
        }catch(e){throw e;}}

    /**
     * An investor is registered on a platfrom and only and operable admin can approve the investor on it's platfrom
     * 
     * @param _admin_acc_id platform admin account id
     * @param _admin_auth_id platform admin account authdescriptor id
     * @param _inv_acc_id investor account id
     * @param _approved approved for a platfrom true/false boolean
     * @param _admin_user platfrom admin user
     */
     public async admin_investor_approved( _admin_acc_id: Buffer, _admin_auth_id: Buffer, _inv_acc_id: Buffer, _approved: boolean, _admin_user: User){try{
        await this._blockchain.call(op('security_token.admin_investor_approved', _admin_acc_id, _admin_auth_id, _inv_acc_id, _approved), _admin_user);
    }catch(e){throw e;}}

    /**
     * A SuperManager who is opreable can add other repredsentatives, default issuing entity representative operable == false
     * The account cannot be registered as another type of user (pp/admin/manager/investor/etc)
     *  
     * @param _manager_acc_id issuing_entity super manager account id
     * @param _manager_auth_id issuing_entity super manager authdescriptor id
     * @param _rep_acc_id issuing entity representative account id
     * @param _rep_title "SuperManager"/"Manager"/"Editor"
     * @param _ie_rep_user signer of tx
     */
    public async manager_add_ie_rep( _manager_acc_id: Buffer, _manager_auth_id: Buffer, _rep_acc_id: Buffer, _rep_title: string, _ie_rep_user: User){try{
            await this._blockchain.call(op('security_token.manager_add_ie_rep', _manager_acc_id, _manager_auth_id, _rep_acc_id, _rep_title), _ie_rep_user);
        }catch(e){throw e;}}

    /**
     * A superManager that is oprable itself can change other representatives operable
     * 
     * @param _supermanager_acc_id issuing_entity super manager account id
     * @param _supermanager_auth_id issuing_entity super manager authdescriptor id
     * @param _rep_acc_id issuing entity representative account id
     * @param _operable enable/disable issuing entity representative, boolean
     * @param _ie_rep_user signer of tx
     */
    public async manager_ie_rep_operable( _supermanager_acc_id: Buffer, _supermanager_auth_id: Buffer, _rep_acc_id: Buffer, _operable: boolean, _ie_rep_user: User){try{
            await this._blockchain.call(op('security_token.manager_ie_rep_operable', _supermanager_acc_id, _supermanager_auth_id, _rep_acc_id, _operable), _ie_rep_user);
        }catch(e){throw e;}}

    /**
     * SuperManager or Manager who are operable can add assets
     * 
     * @param _manager_acc_id issuing_entity super manager or manager account id
     * @param _manager_auth_id issuing_entity super manager or manager authdescriptor id
     * @param _name asset name
     * @param _nr_of_shares number of shares
     * @param _initv intial total value of the full asset (or all shares combined)
     * @param _cur In what currency EUR/USD/SEK/USDT/BTC/ETH for example
     * @param _start_date when campaign starts
     * @param _meta meta data about asset, json string, example: '{"foo":"bar"}'
     * @param _ie_rep_user signer of tx
     */
    public async manager_add_asset( _manager_acc_id: Buffer, _manager_auth_id: Buffer, _name: string, _nr_of_shares: number, _initv: string, _cur: string, _start_date: number, _meta: string, _ie_rep_user: User){try{
            await this._blockchain.call(op('security_token.add_asset', _manager_acc_id, _manager_auth_id, _name, _nr_of_shares, _initv, _cur, _start_date, _meta), _ie_rep_user);
        }catch(e){throw e;}}

    /**
     * SuperManager or Manager who are opreable can change asset oprabel
     * 
     * @param _manager_acc_id issuing_entity super manager or manager account id
     * @param _manager_auth_id issuing_entity super manager or manager authdescriptor id
     * @param _asset asset id
     * @param _operable enable/disable asset, boolean
     * @param _ie_rep_user signer of tx
     */
    public async manager_asset_operable( _manager_acc_id: Buffer, _manager_auth_id: Buffer, _asset: number, _operable: boolean, _ie_rep_user: User){try{
            await this._blockchain.call(op('security_token.manager_asset_operable', _manager_acc_id, _manager_auth_id, _asset, _operable), _ie_rep_user);
        }catch(e){throw e;}}

    /**
     * Register an investor on a platform, an investor can be registerd on multiple platforms
     * The investor has to be approved by an platfrom admin 
     * Default investor/platform.approved == false
     * The account cannot be registered as another type of user (pp/admin/manager/investor/etc)
     * 
     * @param _inv_acc_id investor account id
     * @param _platform platform id
     * @param _user signer of tx (investor itself)
     */
    public async add_investor( _inv_acc_id: Buffer, _platform: string, _user: User){try{
            await this._blockchain.call(op('security_token.add_investor', _inv_acc_id, _platform), _user);
        }catch(e){throw e;}}

    /**
     * When payment and everything is settled the asset shares can be transfered to the Investor
     * only wokrs if all entities and useras are set to operable == true
     * 
     * @param _manager_acc_id issuing_entity super manager account id
     * @param _manager_auth_id issuing_entity super manager authdescriptor id
     * @param _inv_acc_id investor account id
     * @param _asset asset id
     * @param _quantity number of shares to transfer
     * @param _ie_rep_user signer of tx "SuperManager"/"Manager"/"Editor"
     */
    public async initial_transfer_to_investor( _manager_acc_id: Buffer, _manager_auth_id: Buffer, _inv_acc_id: Buffer, _asset: number, _quantity: number, _ie_rep_user: User){try{
            await this._blockchain.call(op('security_token.initial_transfer_to_investor', _manager_acc_id, _manager_auth_id, _inv_acc_id, _asset, _quantity), _ie_rep_user);
        }catch(e){throw e;}}



    public async get_platform_id_by_name(_name:string) {try{
            return await this._gtx.query('security_token.get_platform_id_by_name', {_name: _name});
        }catch(e){throw e;}}
        
    public async get_issuing_entity_id_by_name(_name:string) {try{
            return await this._gtx.query('security_token.get_ie_id_by_name', {_name: _name});
        }catch(e){throw e;}}

    public async get_asset_id_by_name(_name:string) {try{
            return await this._gtx.query('security_token.get_asset_id_by_name', {_name: _name});
        }catch(e){throw e;}}

    public async get_platform(_platform:number) {try{
            return await this._gtx.query('security_token.get_platform', {_platform: _platform});
        }catch(e){throw e;}}

    public async get_issuing_entity(_issuing_entity:number) {try{
            return await this._gtx.query('security_token.get_issuing_entity', {_issuing_entity: _issuing_entity});
        }catch(e){throw e;}}
        
    public async get_asset(_asset:number) {try{
            return await this._gtx.query('security_token.get_asset', {_asset: _asset});
        }catch(e){throw e;}}

    public async get_platform_admin(_acc_id: Buffer) {try{
            return await this._gtx.query('security_token.get_platform_admin', {_acc_id: _acc_id});
        }catch(e){throw e;}}

    public async get_ie_rep(_acc_id: Buffer) {try{
            return await this._gtx.query('security_token.get_ie_rep', {_acc_id: _acc_id});
        }catch(e){throw e;}}

    public async get_investor(_acc_id: Buffer) {try{
            return await this._gtx.query('security_token.get_investor', {_acc_id: _acc_id});
        }catch(e){throw e;}}
}


export {security_token};