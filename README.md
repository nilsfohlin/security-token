npm package for securitytoken rell code.

to use:

```npm install```

```npm link```

in your projecet where you want to use the package

```npm link 2security-token```

```
import {security_token} from '2security-token'

let conf = {blockchainRID : "blockchainRID string", blockchainURI : "blockchainURL string"};

const sdk = new security_token(conf);
await sdk.init();

```


To register a user on the dappchain it has to be an ft3-acc, in other words in the real world the vault would register the account but for dev purposes we can imitate the process by using:

```
await sdk.dev(user.authDescriptor, user);
```
where a user is created by the following 

```
import pcl from 'postchain-client';
import { SingleSignatureAuthDescriptor, FlagsType, User } from 'ft3-lib';

const PRIV = Buffer.from('544113b541f342a268c369a96f54d7e08388199fcc97639753a8cf73f58c39cb', 'hex');
const PUB = Buffer.from('0335cdc891678ba6e91c82b82f5b5e3ab97adcf9e2fb8a3ff4d1c9afb72d9e19db', 'hex');
const keyPair = pcl.util.makeKeyPair();
keyPair.pubKey = PUB;
keyPair.privKey = PRIV;
const authDescriptor = new SingleSignatureAuthDescriptor(keyPair.pubKey, [FlagsType.Account, FlagsType.Transfer]);
export const user = new User(keyPair, authDescriptor);
```


